# ESP8266 weather station

This project is about a weather station or environmental measuring unit built with:

* an EXP8266 based board (either an Oak by Digistump or a WeMos D1 mini)
* BME280 environmental sensor

It uses [ESP8266 core for Arduino](https://github.com/esp8266/Arduino).

## TODO
* deep sleep for ESP8266
* transmission of data (MQTT, REST)

## Dependencies

The dependencies are:

* [BME280](http://platformio.org/lib/show/901/BME280)
    * Wire
    * I2C

## Getting and building the project

### Preparing and installing platformio

```bash
sudo usermod -a -G dialout yourusername
sudo apt-get install python python-pip
pip install --upgrade pip
pip install -U platformio
```

#### Installing 99-platformio-udev.rules

```bash
wget https://github.com/platformio/platformio-core/blob/develop/scripts/99-platformio-udev.rules
sudo mv 99-platformio-udev.rules /etc/udev/rules.d/99-platformio-udev.rules
sudo service udev restart
```

[Installing platformio CLI](http://docs.platformio.org/en/latest/installation.html)

### Getting the source

```bash
git clone https://gafnerpa@bitbucket.org/gafnerpa/esp8266_weather_station.git
cd esp8266_weather_station
git checkout develop
```
### Preparing the project for the IDE
For a complete reference of supported IDEs have a look at [platformio standalone-ide](http://docs.platformio.org/en/latest/ide.html#standalone-ide)

```bash
pio init --ide clion
```

## Compiling and uploading the source

Compile and build the project
```bash
pio run
```

Upload the project
```bash
pio run -t upload
```

## Sensor libraries
* https://github.com/5orenso/arduino-libs/tree/master/Bme280Sensor
* https://github.com/finitespace/BME280

## Alternative sources for library
* [bme280](https://github.com/BoschSensortec/BME280_driver)

## Sensor calibration
The BME280 offers oversampling, filtering and deep sleep methods to optimize the accuracy and power consumptions for the specific needs of the project.
For details refer to the samples of the [BME280 library](https://github.com/finitespace/BME280/blob/master/examples/BME280_Modes/BME280_Modes.ino) or the [official BME280 data sheet](https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME280_DS001-11.pdf).